﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace SpatialIndex
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var reader = new StreamReader(@"C:\Users\wojci\.git\spatial-index-in-.net-standard\SpatialIndex\test\worldcitiespop.csv"))
            {
                List<Point<double>> _points = new List<Point<double>>();
                while (!reader.EndOfStream )
                {
                    var line = reader.ReadLine();

                    var values = line.Split(','); //"Country,City,AccentCity,Region,Population,Latitude,Longitude"
                    if (values[6].Equals("Longitude") || !values[0].Equals("pl"))
                        continue;
                    Point<double> newPoint = 
                        new Point<double>(double.Parse(values[6], CultureInfo.InvariantCulture), float.Parse(values[5], CultureInfo.InvariantCulture));
                    _points.Add(newPoint);
                }
                KDBush<double> _kdbush = new KDBush<double>();
                _kdbush.Index(_points);
                var _result = _kdbush.kClosest(15, 50, 5000, -1);
            }

            KDBush<double> kdbush = new KDBush<double>();
            List<Point<double>> points = new List<Point<double>>(){
                new Point<double>(54, 1, 0),
                new Point<double>(97, 21, 1),
                new Point<double>(25, 37, 0)
            };

            kdbush.Index(points);
            var result = kdbush.kClosest(20, 30,2,-1);

            Console.WriteLine("Done");
        }
    }
}
