﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpatialIndex
{
    /// <summary>
    /// A very fast static spatial index for 2D points based on a flat KD-tree. 
    /// Unlike R-tree or R-bush, KD-tree supports points only and is static 
    /// (can't add points once the index is constructed).
    /// However, the queries are significantly (5-8x) faster
    /// </summary>
    public class KDBush<T>
    {
        internal readonly int nodeSize;
        internal List<Point<T>> points;
        internal int[] ids;
        private SpatialKD spatialKD;
        int earthRadius = 6371;
        double rad = Math.PI / 180;

        /// <summary>
        /// Initialize a KDBush
        /// </summary>
        /// <param name="nodeSize">Size of the KD-tree node, 64 by default. Higher means faster indexing but slower search, and vise versa.</param>
        public KDBush(int nodeSize = 64)
        {
            this.nodeSize = nodeSize;
            this.points = new List<Point<T>>();
            ids = new int[points.Count];
            spatialKD = new SpatialKD();
        }

        /// <summary>
        /// Create an index from the given nodes.
        /// Note, if the index was previously constructed with other points,
        /// those points will be erased
        /// </summary>
        /// <param name="points">List of points to index</param>
        public void Index(IEnumerable<Point<T>> points)
        {
            this.points.Clear();
            this.points.AddRange(points);

            Sort(0, this.points.Count - 1, 0);
        }

        /// <summary>
        /// Query the index for nodes with a rectangle.
        /// Points that lie exactly on the edge of the rectangle are
        /// considered to be within the rectangle and will be returned
        /// </summary>
        /// <param name="x1">min x</param>
        /// <param name="y1">min y</param>
        /// <param name="x2">max x</param>
        /// <param name="y2">max y</param>
        public List<Point<T>> Query(double x1, double y1, double x2, double y2)
        {
            List<Point<T>> hitPoints = new List<Point<T>>();

            Stack<SearchState> stack = new Stack<SearchState>();
            stack.Push(new SearchState(0, points.Count - 1, SearchAxis.XAxis));
            double x;
            double y;

            while (stack.Count > 0)
            {
                SearchState state = stack.Pop();

                if (state.End - state.Start <= nodeSize)
                {
                    for (var i = state.Start; i <= state.End; i++)
                    {
                        x = points[i].X;
                        y = points[i].Y;

                        if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
                        {
                            hitPoints.Add(points[i]);
                        }
                    }

                    continue;
                }

                int m = (state.Start + state.End) / 2;

                x = points[m].X;
                y = points[m].Y;

                if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
                {
                    hitPoints.Add(points[m]);
                }

                SearchAxis nextAxis = (state.Axis == SearchAxis.XAxis) ? SearchAxis.YAxis : SearchAxis.XAxis;

                if ((state.Axis == SearchAxis.XAxis) ? x1 <= x : y1 <= y)
                {
                    stack.Push(new SearchState(state.Start, m - 1, nextAxis));
                }
                if ((state.Axis == SearchAxis.XAxis) ? x2 >= x : y2 >= y)
                {
                    stack.Push(new SearchState(m + 1, state.End, nextAxis));
                }
            }

            return hitPoints;
        }

        /// <summary>
        /// Query the index for nodes with a circle.
        /// Points that lie exactly on the edge of the circle are
        /// considered to be within the circle and will be returned
        /// </summary>
        /// <param name="x">center X of the circle</param>
        /// <param name="y">center Y of the circle</param>
        /// <param name="radius">radius of the circle</param>
        public List<Point<T>> Query(double x, double y, double radius)
        {
            List<Point<T>> hitPoints = new List<Point<T>>();
            Stack<SearchState> stack = new Stack<SearchState>();
            stack.Push(new SearchState(0, points.Count - 1, SearchAxis.XAxis));
            double r2 = radius * radius;

            while (stack.Count > 0)
            {
                SearchState state = stack.Pop();

                if ((state.End - state.Start) <= nodeSize)
                {
                    for (var i = state.Start; i <= state.End; i++)
                    {
                        if (SquareDistance(points[i].X, points[i].Y, x, y) <= r2)
                        {
                            hitPoints.Add(points[i]);
                        }
                    }

                    continue;
                }

                int m = (state.Start + state.End) / 2;

                double px = points[m].X;
                double py = points[m].Y;

                if (SquareDistance(px, py, x, y) <= r2)
                {
                    hitPoints.Add(points[m]);
                }

                SearchAxis nextAxis = (state.Axis == SearchAxis.XAxis) ? SearchAxis.YAxis : SearchAxis.XAxis;

                if ((state.Axis == SearchAxis.XAxis) ? x - radius <= px : y - radius <= py)
                {
                    stack.Push(new SearchState(state.Start, m - 1, nextAxis));
                }
                if ((state.Axis == SearchAxis.XAxis) ? x + radius >= px : y + radius >= py)
                {
                    stack.Push(new SearchState(m + 1, state.End, nextAxis));
                }
            }

            return hitPoints;
        }

        public List<Point<T>> kClosest(double x, double y, int k, int maxDistance)
        {
            List<Point<T>> hitPoints = new List<Point<T>>();
            Stack<SearchState> stack = new Stack<SearchState>();
            SpatialNodeComp spatialNodeComp = new SpatialNodeComp();
            stack.Push(new SearchState(0, points.Count - 1, SearchAxis.XAxis));
            List<SpatialNode> heap = new List<SpatialNode>();
            double maxHaverSinDist = 0;
            double cosLat = Math.Cos(x * rad);


            if (maxDistance != -1)
            {
                maxHaverSinDist = spatialKD.haversine(maxDistance / earthRadius);
            }
            SpatialNode node = new SpatialNode(0); // first node
            node.minimumLatitude = -90;
            node.maximumLatitude = 90;
            node.maximumLongitude = 180;
            node.minimumLongitude = -180;
            node.leftIndex = 0;
            node.rightIndex = points.Count - 1;


            while (stack.Count > 0)
            {
                int left = node.leftIndex;
                int right = node.rightIndex;
                SearchState state = stack.Pop();
                int m = (state.Start + state.End) / 2;
                if ((state.End - state.Start) <= nodeSize) //leaf node
                {
                    for (var i = state.Start; i <= state.End; i++) // add all points of the leaf node to the queue
                    {
                        SpatialNode leafNode = new SpatialNode(spatialKD.haversineDistance(x, y, points[i].X, points[i].Y, cosLat));
                        leafNode.latitude = points[i].Y;
                        leafNode.longitude = points[i].X;
                        leafNode.isNode = false;

                        heap.Add(leafNode);
                    }


                }
                else {

                    //middle index

                    double midx = points[m].X;
                    double midy = points[m].Y;

                    SpatialNode midNode = new SpatialNode(spatialKD.haversineDistance(x, y, midx, midy, cosLat));
                    midNode.latitude = midy;
                    midNode.longitude = midx;
                    midNode.isNode = true;
                    heap.Add(midNode);

                    SpatialNode leftNode = new SpatialNode(0);
                    leftNode.maximumLatitude = node.maximumLatitude;
                    leftNode.minimumLatitude = node.minimumLatitude;
                    leftNode.maximumLongitude = node.maximumLongitude;
                    leftNode.minimumLongitude = node.minimumLongitude;
                    leftNode.isNode = true;
                    leftNode.leftIndex = left;
                    leftNode.rightIndex = m - 1;
                    leftNode.distance = spatialKD.boxDist(x, y, cosLat, leftNode);

                    SpatialNode rightNode = new SpatialNode(0);
                    rightNode.maximumLatitude = node.maximumLatitude;
                    rightNode.minimumLatitude = node.minimumLatitude;
                    rightNode.maximumLongitude = node.maximumLongitude;
                    rightNode.minimumLongitude = node.minimumLongitude;
                    rightNode.isNode = true;
                    rightNode.leftIndex = m + 1;
                    rightNode.rightIndex = right;
                    rightNode.distance = spatialKD.boxDist(x, y, cosLat, rightNode);

                    heap.Add(leftNode); // add child nodes to the queue
                    heap.Add(rightNode);
                }

                heap.Sort();
                int index = heap.Count - 1;

                // fetch closest points from the queue; they're guaranteed to be closer
                // than all remaining points (both individual and those in kd-tree nodes),
                // since each node's distance is a lower bound of distances to its children
                while (heap.Count != 0)
                {
                    SpatialNode candidate = heap[index];
                    if (candidate.isNode)
                    {
                        break;
                    } else
                    {
                        heap.RemoveAt(index);
                        if (maxDistance != -1 && candidate.distance > maxHaverSinDist)
                        {
                            return hitPoints;
                        }
                        Point<T> newPoint = new Point<T>(candidate.longitude,candidate.latitude);

                        hitPoints.Add(newPoint);
                        if(hitPoints.Count == k)
                        {
                            return hitPoints;
                        }
                    }
                    index--;
                }


                SearchAxis nextAxis = (state.Axis == SearchAxis.XAxis) ? SearchAxis.YAxis : SearchAxis.XAxis;
                SpatialNode _candidate = heap[index];

                stack.Push(new SearchState(_candidate.leftIndex,_candidate.rightIndex, nextAxis));
                node = _candidate;
                heap.RemoveAt(index);
            }
            return hitPoints;
        }

        private void Sort(int left, int right, int depth)
        {
            if ((right - left) <= nodeSize)
            {
                return;
            }

            int m = (left + right) / 2;

            Select(m, left, right, depth % 2);

            Sort(left, m - 1, depth + 1);
            Sort(m + 1, right, depth + 1);
        }

        private void Select(int k, int left, int right, int inc)
        {
            while (right > left)
            {
                if ((right - left) > 600)
                {
                    int n = right - left + 1;
                    int m = k - left + 1;
                    double z = Math.Log(n);

                    double s = 0.5 * Math.Exp(2 * z / 3);
                    double sd = 0.5 * Math.Sqrt(z * s * (n - s) / n) * (m - n / 2 < 0 ? -1 : 1);
                    int newLeft = (int)Math.Max(left, (k - m * s / n + sd));
                    int newRight = (int)Math.Min(right, (k + (n - m) * s / n + sd));

                    Select(k, newLeft, newRight, inc);
                }

                Func<int, double> getCoordinate;

                if (inc == 0)
                {
                    getCoordinate = (int index) => { return points[index].X; };
                }
                else
                {
                    getCoordinate = (int index) => { return points[index].Y; };
                }

                var t = getCoordinate(k);
                var i = left;
                var j = right;

                Swap(left, k);
                if (getCoordinate(right) > t)
                {
                    Swap(left, right);
                }

                while (i < j)
                {
                    Swap(i, j);
                    i++;
                    j--;

                    while (getCoordinate(i) < t)
                    {
                        i++;
                    }

                    while (getCoordinate(j) > t)
                    {
                        j--;
                    }
                }

                if (getCoordinate(left) == t)
                {
                    Swap(left, j);
                }
                else
                {
                    j++;
                    Swap(j, right);
                }

                if (j <= k)
                {
                    left = j + 1;
                }
                if (k <= j)
                {
                    right = j - 1;
                }
            }
        }

        private void Swap(int i1, int i2)
        {
            Point<T> temp = points[i1];
            points[i1] = points[i2];
            points[i2] = temp;
        }

        private double SquareDistance(double x1, double y1, double x2, double y2)
        {
            double dx = x1 - x2;
            double dy = y1 - y2;

            return (dx * dx) + (dy * dy);
        }

        enum SearchAxis
        {
            XAxis = 0,
            YAxis = 1
        }

        /// <summary>
        /// Class to keep track of search state
        /// </summary>
        class SearchState
        {
            public SearchAxis Axis { get; private set; }
            public int Start { get; private set; }
            public int End { get; private set; }

            public SearchState(int start, int end, SearchAxis axis)
            {
                Axis = axis;
                Start = start;
                End = end;
            }
        }
    }
}
