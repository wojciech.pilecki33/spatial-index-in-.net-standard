﻿using C5;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SpatialIndex
{
    class SpatialKD
    {
        int earthRadius = 6371;
        double rad = Math.PI / 180;

        public double compareDistance(SpatialNode a, SpatialNode b)
        {
            return a.distance - b.distance;
        }

        // lower bound for distance from a location to points inside a bounding box
        public double boxDist(double lng, double lat, double cosLat, SpatialNode node)
        {
            var minLng = node.minimumLongitude;
            var maxLng = node.maximumLongitude;
            var minLat = node.minimumLatitude;
            var maxLat = node.maximumLatitude;

            // query point is between minimum and maximum longitudes
            if (lng >= minLng && lng <= maxLng)
            {
                if (lat < minLat) return haversine((lat - minLat) * rad);
                if (lat > maxLat) return haversine((lat - maxLat) * rad);
                return 0;
            }

            // query point is west or east of the bounding box;
            // calculate the extremum for great circle distance from query point to the closest longitude;
            var haverSinDLng = Math.Min(haversine((lng - minLng) * rad), haversine((lng - maxLng) * rad));
            var extremumLat = vertexLat(lat, haverSinDLng);

            // if extremum is inside the box, return the distance to it
            if (extremumLat > minLat && extremumLat < maxLat)
            {
                return haversinePartial(haverSinDLng, cosLat, lat, extremumLat);
            }
            // otherwise return the distan e to one of the bbox corners (whichever is closest)
            return Math.Min(
                haversinePartial(haverSinDLng, cosLat, lat, minLat),
                haversinePartial(haverSinDLng, cosLat, lat, maxLat)
            );
        }

        //Haversine formula - teramines the great-circle distance between two points on a sphere
        public double haversineDistance(double lng1,double lat1, double lng2, double lat2, double cosLat1)
        {
            double haversineDLng = haversine((lng1 - lng2) * rad);
            return haversinePartial(haversineDLng, cosLat1, lat1, lat2);
        }

        private double haversinePartial(double haversineDLng, double cosLat1, double lat1, double lat2) { 
        
            return cosLat1 * Math.Cos(lat2 * rad) * haversineDLng + haversine((lat1 - lat2) * rad);
        }

        public double haversine(double theta)
        {
            double s = Math.Sin(theta / 2);
            return s * s;
        }

        private double vertexLat(double lat, double haversineDLng)
        {
            double cosDLng = 1 - 2 * haversineDLng;
            if (cosDLng <= 0)
            {
                return lat > 0 ? 90 : -90;
            }
            return Math.Atan(Math.Tan(lat * rad) / cosDLng) / rad;
        }
    }

    public class SpatialNode : IComparable
    {
        public SpatialNode(double distance)
        {
            this.distance = distance;
        }
        public double longitude
        { get; set; }
        public double latitude
        { get; set; }
        public int leftIndex
        { get; set; }
        public int rightIndex
        { get; set; }
        public bool isNode
        { get; set; }
        public double distance
        { get; set; }
        public double minimumLatitude
        { get; set; }
        public double minimumLongitude
        { get; set; }
        public double maximumLatitude
        { get; set; }
        public double maximumLongitude
        { get; set; }

        public int CompareTo(SpatialNode obj)
        {
            return (int) this.distance - (int) obj.distance;
        }

        public int CompareTo(object obj)
        {
            double result = this.distance - ((SpatialNode)obj).distance;
            if (result > 0)
                return -1;
            else if (result < 0)
                return 1;
            else return 0;
        }
    }

    public class SpatialNodeComp : IComparer<SpatialNode>
    {
        public int Compare(SpatialNode a, SpatialNode b)
        {
            return a.distance.CompareTo(b.distance);
        }

    }
}
