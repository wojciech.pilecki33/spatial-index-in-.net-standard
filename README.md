#  Spatial index in .NET standard

Implementation of a multidimensional spatial indexing which can be utilized within GIS and statistics community.

## Description

An implementation and geographic extension for [kdbush](https://github.com/mourner/kdbush), static spatial index for points in JavaScript.

It provides implementation of k-closest points.

Implementation of basic KDBush algorithm in c# can be found [here](https://github.com/marchello2000/kdbush)

## Scheme and implementation

![alt text](https://www.researchgate.net/profile/Siyuan_Chen6/publication/305453238/figure/fig3/AS:538719448821762@1505452133776/K-d-Tree-in-3D-space-CURRENT-FUTURE-DEVELOPMENTS-In-the-current-market-users-can.png)

## Usage

## Tests

Tests performed on a [cities database](https://www.kaggle.com/max-mind/world-cities-database)

